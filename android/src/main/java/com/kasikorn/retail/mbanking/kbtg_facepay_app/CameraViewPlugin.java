package com.kasikorn.retail.mbanking.kbtg_facepay_app;

import io.flutter.plugin.common.PluginRegistry.Registrar;

public class CameraViewPlugin {
    public static void registerWith(Registrar registrar) {
        registrar
                .platformViewRegistry()
                .registerViewFactory(
                        "plugins.felix.angelov/textview", new CameraViewFactory(registrar.messenger()));
    }
}