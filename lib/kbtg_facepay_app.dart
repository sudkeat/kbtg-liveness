import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef void CameraViewCreatedCallback(CameraViewController controller);

//class KbtgFacepayApp {
//  static const MethodChannel _channel = const MethodChannel('kbtg_facepay_app');
//
//  static Future<String> get platformVersion async {
//    final String version = await _channel.invokeMethod('getPlatformVersion');
//    return version;
//  }
//}

class CameraView extends StatefulWidget {
  const CameraView({
    Key key,
    this.onTextViewCreated,
  }) : super(key: key);

  final CameraViewCreatedCallback onTextViewCreated;

  @override
  State<StatefulWidget> createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> {
  @override
  Widget build(BuildContext context) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return AndroidView(
        viewType: 'plugins.felix.angelov/textview',
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    }
    return Text('is not yet supported plugin');
  }

  void _onPlatformViewCreated(int id) {
    if (widget.onTextViewCreated == null) {
      return;
    }
    widget.onTextViewCreated(new CameraViewController._(id));
  }
}

class CameraViewController {
  CameraViewController._(int id) : _channel = new MethodChannel('plugins.felix.angelov/textview_$id');

  final MethodChannel _channel;

  Future<void> detectFace() async {
    bool isSuccessOnly = false;
    return _channel.invokeMethod('detect-face-liveness', isSuccessOnly);
  }

  Future<String> checkStatus() async {
    return _channel.invokeMethod('check-status', null);
  }
  Future<String> pathImage() async {
    return _channel.invokeMethod('path-image', null);
  }
  Future<void> stopCamera() async {
    return _channel.invokeMethod('stop-camera', null);
  }
}
