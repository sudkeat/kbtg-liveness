package com.kasikorn.retail.mbanking.kbtg_facepay_app;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.os.Environment;
import android.os.SystemClock;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;

import com.kasikorn.retail.mbanking.kbtg.SenseCamera;
import com.sensetime.senseid.sdk.liveness.silent.OnLivenessListener;
import com.sensetime.senseid.sdk.liveness.silent.SilentLivenessApi;
import com.sensetime.senseid.sdk.liveness.silent.common.type.PixelFormat;
import com.sensetime.senseid.sdk.liveness.silent.common.type.ResultCode;
import com.sensetime.senseid.sdk.liveness.silent.common.type.Size;
import com.sensetime.senseid.sdk.liveness.silent.common.util.FileUtil;
import com.sensetime.senseid.sdk.liveness.silent.common.util.ImageUtil;
import com.sensetime.senseid.sdk.liveness.silent.type.FaceDistance;
import com.sensetime.senseid.sdk.liveness.silent.type.FaceOcclusion;
import com.sensetime.senseid.sdk.liveness.silent.type.FaceState;
import com.sensetime.senseid.sdk.liveness.silent.type.OcclusionState;

import java.io.File;
import java.util.Date;
import java.util.List;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformView;

public class CameraView implements PlatformView, MethodChannel.MethodCallHandler {
    private final MethodChannel methodChannel;
    private LinearLayout cameraPreview;
    public Context mContext;
    private Camera mCamera;
    private final SurfaceView surfView;
    public SenseCamera mSenseCamera;

    CameraView(Context context, BinaryMessenger messenger, int id) {
        mContext = context;
        System.out.println("CameraView111");
        mCamera = setConfigCamera(context);
        System.out.println("CameraView222");
        mCamera.setDisplayOrientation(90);
        surfView = new CameraPreview(context,mCamera);
        System.out.println("CameraView333");
        cameraPreview = new LinearLayout(context);
        System.out.println("CameraView444");
        cameraPreview.addView(surfView);
        System.out.println("CameraView555");
        mCamera.startPreview();
        System.out.println("CameraView666");

        methodChannel = new MethodChannel(messenger, "plugins.felix.angelov/textview_" + id);
        methodChannel.setMethodCallHandler(this);
    }

    private Camera setConfigCamera(Context context){
        Camera camera = null;
        try{
            mSenseCamera = new SenseCamera.Builder(context).setFacing(SenseCamera.CAMERA_FACING_FRONT)
                    .setRequestedPreviewSize(640, 480)
                    .build();

            camera = mSenseCamera.createCamera();
        }catch (Exception e) {
            System.out.println("Exception " + e);
        }
        return camera;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case "detect-face-liveness":
                detectFace(methodCall, result);
                break;
            case "check-status":
                checkStatus(methodCall, result);
                break;
            case "path-image":
                getPathImage(methodCall, result);
                break;
            case "stop-camera":
                stopCamera(methodCall, result);
                break;
            default:
                result.notImplemented();
        }
    }

    private void setSuccessOnly(boolean isSuccessOnly){
        this.isSuccessOnly = isSuccessOnly;
    }

    private void checkStatus(MethodCall methodCall, MethodChannel.Result result) {
        result.success(resultStatus);
    }

    private void getPathImage(MethodCall methodCall, MethodChannel.Result result) {
        result.success(resultPathImage);
    }

    private void stopCamera(MethodCall methodCall, MethodChannel.Result result) {
        System.out.println("stopCamera 1--->");
        mCamera.setPreviewCallbackWithBuffer(null);
        SilentLivenessApi.stop();
        SilentLivenessApi.release();
        System.out.println("stopCamera 2--->");
        mSenseCamera.stop(mCamera);
        result.success(null);
    }

    private void detectFace(MethodCall methodCall, MethodChannel.Result result) {
        boolean isSuccessOnly = (Boolean) methodCall.arguments;
        System.out.println("isSuccessOnly");
        setSuccessOnly(isSuccessOnly);
        try {
            File dir = new File(FILES_PATH);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            final File protobuf = new File(FILE_POTOBUF_RESULT);
            if (protobuf.exists()) {
                protobuf.delete();
            }

            File image = new File(FILE_IMAGE);
            if (image.exists()) {
                //noinspection ResultOfMethodCallIgnored
                image.delete();
            }
            System.out.println("copyAssetsToFile start");
            FileUtil.copyAssetsToFile(mContext, LICENSE_FILE_NAME, FILES_PATH + LICENSE_FILE_NAME);
            FileUtil.copyAssetsToFile(mContext, DETECTION_MODEL_FILE_NAME, FILES_PATH + DETECTION_MODEL_FILE_NAME);
            FileUtil.copyAssetsToFile(mContext, ALIGNMENT_MODEL_FILE_NAME, FILES_PATH + ALIGNMENT_MODEL_FILE_NAME);
            FileUtil.copyAssetsToFile(mContext, FRAME_SELECTOR_MODEL_FILE_NAME, FILES_PATH + FRAME_SELECTOR_MODEL_FILE_NAME);
            FileUtil.copyAssetsToFile(mContext, ANTI_SPOOFING_MODEL_FILE_NAME, FILES_PATH + ANTI_SPOOFING_MODEL_FILE_NAME);

            System.out.println("FileUtil.copyAssetsToFile");
            SilentLivenessApi.stop();
            SilentLivenessApi.release();
            SilentLivenessApi.init(mContext, FILES_PATH + LICENSE_FILE_NAME,
                    FILES_PATH + DETECTION_MODEL_FILE_NAME, FILES_PATH + ALIGNMENT_MODEL_FILE_NAME,
                    FILES_PATH + FRAME_SELECTOR_MODEL_FILE_NAME, FILES_PATH + ANTI_SPOOFING_MODEL_FILE_NAME,
                    mLivenessListener);
            SilentLivenessApi.setFaceDistanceRate(0.4F, 0.8F);

            System.out.println("SilentLivenessApi.init");

            mCamera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
                @Override
                public void onPreviewFrame(byte[] data, Camera camera) {
                    camera.addCallbackBuffer(mSenseCamera.mBytesToByteBuffer.get(data).array());
                    onProcessFrame(mSenseCamera.mBytesToByteBuffer.get(data).array(), camera);
                }
            });
        }catch (Exception e){
            System.out.println("Exception " + e);
        }

        result.success(null);
    }

    @Override
    public View getView() {
        return cameraPreview;
    }

    @Override
    public void dispose() {

    }

    public void onProcessFrame(byte[] data, Camera camera) {
        System.out.println("onPreviewFrame--->");
        if (!this.mStartInputData) {
            return;
        }

        System.out.println("onPreviewFrame length ---> " + data.length);
        final int width = mSenseCamera.getPreviewSize().getWidth();
        final int height = mSenseCamera.getPreviewSize().getHeight();
        System.out.println("onPreviewFrame width ---> " + width);
        System.out.println("onPreviewFrame height ---> " + height);
        System.out.println("onPreviewFrame s.getRotationDegrees() ---> " + mSenseCamera.getRotationDegrees());

//        try {
//            Date date = new Date();
//            long time = date.getTime();
//            FileOutputStream fos = new FileOutputStream("/storage/emulated/0/Download/yuv/" + time + "test.jpg");
//            YuvImage yuvImage = new YuvImage(data, ImageFormat.NV21, width, height, null);
//            yuvImage.compressToJpeg(new Rect(0, 0, width, height), 100, fos);
//            fos.close();
//        }catch (Exception ex) {
//            System.out.println("Exception " + ex);
//        }

        SilentLivenessApi.inputData(data, PixelFormat.NV21, new Size(width, height),
                convertViewRectToPicture(getMaskBounds()), true,
                mSenseCamera.getRotationDegrees());
    }

    public Rect getMaskBounds() {
//        int viewWidth = this.s.getPreviewSize().getWidth();
//        int viewHeight = this.s.getPreviewSize().getHeight();
        final int viewWidth = 1080;
        final int viewHeight = 1440;
        final Rect rect = new Rect();
        this.mRectF.set(viewWidth * 0.1f, viewHeight * 0.1f, viewWidth * 0.9f, viewHeight * 0.9f);
        this.mRectF.round(rect);
        return rect;
    }

    public Rect convertViewRectToPicture(final Rect viewRect) {
//        final int viewWidth = this.s.getPreviewSize().getWidth();
//        final int viewHeight = this.s.getPreviewSize().getHeight();
        final int viewWidth = 1080;
        final int viewHeight = 1440;

        System.out.println("onPreviewFrame viewWidth ---> " + viewWidth);
        System.out.println("onPreviewFrame viewHeight ---> " + viewHeight);
        System.out.println("onPreviewFrame bottom ---> " + viewRect.bottom);
        System.out.println("onPreviewFrame left ---> " + viewRect.left);
        System.out.println("onPreviewFrame right ---> " + viewRect.right);
        System.out.println("onPreviewFrame top ---> " + viewRect.top);

        final int cameraRotationDegrees = this.mSenseCamera.getRotationDegrees();
        final int imageWidth = this.mSenseCamera.getPreviewSize().getWidth();
        final int imageHeight = this.mSenseCamera.getPreviewSize().getHeight();

        float widthRatio;
        float heightRatio;

        switch (cameraRotationDegrees) {
            case 90:
            case 270:
                widthRatio = (float) imageHeight / viewWidth;
                heightRatio = (float) imageWidth / viewHeight;
                break;
            case 0:
            case 180:
            default:
                widthRatio = (float) imageWidth / viewWidth;
                heightRatio = (float) imageHeight / viewHeight;
                break;
        }

        final float scale = widthRatio < heightRatio ? widthRatio : heightRatio;

        scaledRect = new Rect((int) (viewRect.left * scale + 0.5f), (int) (viewRect.top * scale + 0.5f),
                (int) (viewRect.right * scale + 0.5f), (int) (viewRect.bottom * scale + 0.5f));

        Rect rotateRect = new Rect(scaledRect);
        switch (cameraRotationDegrees) {
            case 90:
                rotateRect = new Rect(scaledRect.top, imageHeight - scaledRect.right, scaledRect.bottom,
                        imageHeight - scaledRect.left);
                break;
            case 180:
                rotateRect = new Rect(imageWidth - scaledRect.right, imageHeight - scaledRect.bottom,
                        imageWidth - scaledRect.left, imageHeight - scaledRect.top);
                break;
            case 270:
                rotateRect = new Rect(imageWidth - scaledRect.bottom, scaledRect.left, imageWidth - scaledRect.top,
                        scaledRect.right);
                break;
            case 0:
            default:
                break;
        }

        Rect resultRect = new Rect(rotateRect);
        if (this.mSenseCamera.getCameraFacing() == SenseCamera.CAMERA_FACING_FRONT) {
            switch (cameraRotationDegrees) {
                case 90:
                case 270:
                    resultRect = new Rect(rotateRect.left, imageHeight - rotateRect.bottom, rotateRect.right,
                            imageHeight - rotateRect.top);
                    break;
                case 0:
                case 180:
                    resultRect = new Rect(imageWidth - rotateRect.right, rotateRect.top, imageWidth - rotateRect.left,
                            rotateRect.bottom);
                    break;
                default:
                    break;
            }
        }

        return resultRect;
    }

    public static final int CANCEL_INITIATIVE = 0x101;
    public static final String RESULT_DEAL_ERROR_INNER = "result_deal_error_inner";
    public static final String RESULT_FACE_RECT = "result_face_rect";
    public static final String RESULT_INFO = "result_info";
    public static final String FILES_PATH = Environment.getExternalStorageDirectory().getPath() + "/sensetime/";
    public static final String LICENSE_FILE_NAME = "SenseID_Liveness_Silent.lic";
    public static final String FILE_IMAGE = FILES_PATH + "silent_liveness/silent_liveness_image.jpg";
    public static final String FILE_POTOBUF_RESULT = FILES_PATH + "silent_liveness/silentLivenessResult";

    public static final String DETECTION_MODEL_FILE_NAME = "M_Detect_Hunter_SmallFace.model";
    public static final String ALIGNMENT_MODEL_FILE_NAME = "M_Align_occlusion.model";
    public static final String FRAME_SELECTOR_MODEL_FILE_NAME = "M_Liveness_Cnn_half.model";
    public static final String ANTI_SPOOFING_MODEL_FILE_NAME = "M_Liveness_Antispoofing.model";

    public boolean mStartInputData = false;
    public boolean mIsCanceled = true;
    public Rect scaledRect = null;
    private RectF mRectF = new RectF();
    private boolean isSuccessOnly;
    private String resultPathImage = null;
    private String resultStatus = null;

    private OnLivenessListener mLivenessListener = new OnLivenessListener() {

        private long lastStatusUpdateTime;

        @Override
        public void onInitialized() {
            System.out.println("SilentLivenessApi.start()");
            SilentLivenessApi.start();
            mStartInputData = true;
        }

        @Override
        public void onStatusUpdate(final int faceState, final FaceOcclusion faceOcclusion, final int faceDistance) {
            System.out.println("onStatusUpdate faceState --->"+faceState);
            System.out.println("onStatusUpdate faceDistance --->"+faceDistance);
            if (SystemClock.elapsedRealtime() - this.lastStatusUpdateTime < 300 && faceState != FaceState.NORMAL) {
                return;
            }
            System.out.println("faceState--->"+faceState);
            if (faceState == FaceState.MISSED) {
//                mTipsView.setText(R.string.common_tracking_missed);
            } else if (faceDistance == FaceDistance.TOO_CLOSE) {
//                mTipsView.setText(R.string.common_face_too_close);
            } else if (faceState == FaceState.OUT_OF_BOUND) {
//                mTipsView.setText(R.string.common_tracking_out_of_bound);
            } else if (faceState == FaceState.OCCLUSION) {
                final StringBuilder builder = new StringBuilder();
                boolean needComma = false;
                if (faceOcclusion.getBrowOcclusionState() == OcclusionState.OCCLUSION) {
//                    builder.append(SilentLivenessActivity.this.getString(R.string.common_tracking_covered_brow));
                    needComma = true;
                }
                if (faceOcclusion.getEyeOcclusionState() == OcclusionState.OCCLUSION) {
                    builder.append(needComma ? "、" : "");
//                    builder.append(SilentLivenessActivity.this.getString(R.string.common_tracking_covered_eye));
                    needComma = true;
                }
                if (faceOcclusion.getNoseOcclusionState() == OcclusionState.OCCLUSION) {
                    builder.append(needComma ? "、" : "");
//                    builder.append(SilentLivenessActivity.this.getString(R.string.common_tracking_covered_nose));
                    needComma = true;
                }
                if (faceOcclusion.getMouthOcclusionState() == OcclusionState.OCCLUSION) {
                    builder.append(needComma ? "、" : "");
//                    builder.append(SilentLivenessActivity.this.getString(R.string.common_tracking_covered_mouth));
                }

//                mTipsView.setText( SilentLivenessActivity.this.getString(R.string.common_tracking_covered, builder.toString()));
            } else if (faceDistance == FaceDistance.TOO_FAR) {
//                mTipsView.setText(R.string.common_face_too_far);
            } else {
//                mTipsView.setText(R.string.common_detecting);
            }

            this.lastStatusUpdateTime = SystemClock.elapsedRealtime();
        }

        @Override
        public void onError(ResultCode resultCode) {

            System.out.println("onError#####################--->"+resultCode);
            mStartInputData = false;
            mIsCanceled = false;

            resultStatus = "F";
            final Intent data = new Intent();
            switch (resultCode) {
                case STID_E_DETECT_FAIL:
                    /*Start:Codes for rebegin.
                    SilentLivenessActivity.this.reBegin(resultCode);
                    return;
                    End*/

                    /*Start: codes for normal detection.*/
                    data.putExtra(RESULT_INFO, resultCode);
                    data.putExtra(RESULT_DEAL_ERROR_INNER, false);
//                    setResult(ActivityUtils.convertResultCode(resultCode), data);
                    break;
                /*End*/
                default:
                    data.putExtra(RESULT_DEAL_ERROR_INNER, true);
//                    SilentLivenessActivity.this.showError(getErrorNotice(resultCode));
//                    setResult(ActivityUtils.convertResultCode(resultCode), data);
                    break;
            }
//            finish();
        }

        @Override
        public void onDetectOver(ResultCode resultCode, byte[] result, List imageData, Rect faceRect) {
            System.out.println("onDetectOver###############################--->"+resultCode);
            System.out.println("onDetectOver###############################--->"+imageData.size());
            mStartInputData = false;
            mIsCanceled = false;

            //Codes to save protobuf data.
            if (result != null && result.length > 0) {
                FileUtil.saveDataToFile(result, FILE_POTOBUF_RESULT);
            }

            Date date = new Date();
            long time = date.getTime();

            //Codes to save image results.
            List<byte[]> imageResult = (List<byte[]>) imageData;
//            String pathImage = "/storage/emulated/0/Download/yuv/" + time + "test.jpg";
            String pathImage = FILES_PATH + "silent_liveness/" + time + "test.jpg";
            if (imageResult != null && !imageResult.isEmpty()) {
                ImageUtil.saveBitmapToFile(
                        BitmapFactory.decodeByteArray(imageResult.get(0), 0, imageResult.get(0).length), pathImage);
            }

            //Codes for normal detection.
            final Intent data = new Intent();
            data.putExtra(RESULT_DEAL_ERROR_INNER, false);
            switch (resultCode) {
                case OK:
                    if (imageResult != null && !imageResult.isEmpty() && faceRect != null) {
                        data.putExtra(RESULT_FACE_RECT, faceRect);
                    }
                    resultStatus = "S";
                    resultPathImage = pathImage;
//                    setResult(RESULT_OK, data);
                    break;
                default:
                    resultStatus = "F";
                    /*Start: codes for normal detection.*/
                    data.putExtra(RESULT_INFO, resultCode);
//                    setResult(ActivityUtils.convertResultCode(resultCode), data);
                    break;
                /*End*/

                    /*Start:Codes for rebegin.
                    SilentLivenessActivity.this.reBegin(resultCode);
                    return;
                    End*/
            }
//            finish();
        }
    };
}